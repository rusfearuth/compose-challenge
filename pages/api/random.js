const dogs = [
  {
    id: 1,
    imageUrl: 'https://random.dog/63bb7640-708d-4bba-a6d5-af527b94254b.jpg',
  },
  {
    id: 2,
    imageUrl: 'https://random.dog/1ddb2caa-41f5-456f-a446-f5a5335b5811.jpg',
  },
  {
    id: 3,
    imageUrl: 'https://random.dog/f996a8dd-b310-400e-95d4-9dd843a97c58.jpg',
  },
  {
    id: 4,
    imageUrl: 'https://random.dog/c6c7ab71-bf8c-4c1d-8bee-31cbdf5e2f5e.jpg',
  },
  {
    id: 5,
    imageUrl: 'https://random.dog/751f84b1-524f-4da7-b6f6-aadae5ef9cc1.jpeg',
  },
  {
    id: 6,
    imageUrl: 'https://random.dog/165a3967-976c-4304-81d5-5a76f4e9b8d2.jpg',
  },
  {
    id: 7,
    imageUrl: 'https://random.dog/56217498-0e6b-4c24-bdd1-cc5dbb2201bb.jpg',
  },
  {
    id: 8,
    imageUrl: 'https://random.dog/07c4e089-a7da-430a-ac09-a0da419efefa.JPG',
  },
  {
    id: 9,
    imageUrl: 'https://random.dog/5d9b4f1f-ed47-47d2-95b4-ac58ac0834b7.jpg',
  },
  {
    id: 10,
    imageUrl: 'https://random.dog/f856b11f-6d47-4089-9edf-aad8107161d0.jpg',
  },
  {
    id: 11,
    imageUrl: 'https://random.dog/fa68ed75-f7e0-44eb-a39b-408b984d2f90.jpg',
  },
  {
    id: 12,
    imageUrl: 'https://random.dog/d0bdedc7-3dd2-4d3a-ac92-dfc3a69fe27f.jpg',
  },
  {
    id: 13,
    imageUrl: 'https://random.dog/5cd73551-c42b-4c0f-bd80-634998e9a128.jpg',
  },
  {
    id: 14,
    imageUrl: 'https://random.dog/27ee3890-8eca-4038-b77e-b6630ebd3b74.jpg',
  },
  {
    id: 15,
    imageUrl: 'https://random.dog/c8b29dd6-951a-4444-9edd-e174adb1c7ab.jpg',
  },
  {
    id: 16,
    imageUrl: 'https://random.dog/099ee7e2-6ad5-4f50-bc0f-2ac192620fc7.jpg',
  },
  {
    id: 17,
    imageUrl: 'https://random.dog/027eef85-ccc1-4a66-8967-5d74f34c8bb4.jpg',
  },
  {
    id: 18,
    imageUrl: 'https://random.dog/9e90f135-67ec-47ef-9d99-61b9223f2e86.jpg',
  },
  {
    id: 19,
    imageUrl: 'https://random.dog/914e15e9-ddf2-4b7a-b380-0aa9ff7458e7.PNG',
  },
  {
    id: 20,
    imageUrl: 'https://random.dog/8b28a6b8-2711-47ef-b8b8-fd557464a1ed.jpg',
  },
  {
    id: 21,
    imageUrl: 'https://random.dog/440f11bf-b9d4-44de-a5c3-3f0893b04fa2.jpg',
  },
  {
    id: 22,
    imageUrl: 'https://random.dog/47e0db30-c4a4-4719-97c4-54ed5893934f.JPG',
  },
  {
    id: 23,
    imageUrl: 'https://random.dog/0886de63-9a5d-41c5-b750-4e7633f63ce1.jpg',
  },
  {
    id: 24,
    imageUrl: 'https://random.dog/068fc183-d4e3-4780-b01c-6cce0d019d13.jpg',
  },
  {
    id: 25,
    imageUrl: 'https://random.dog/25b868be-63d4-4b59-a695-2a7719a4dcb6.jpg',
  },
  {
    id: 26,
    imageUrl: 'https://random.dog/813c353e-7c6b-45d9-a756-af99878133d7.jpg',
  },
  {
    id: 27,
    imageUrl: 'https://random.dog/43978206-a479-4753-ad33-22e16c0f7865.JPG',
  },
  {
    id: 28,
    imageUrl: 'https://random.dog/c05eb611-4b3f-46fc-8464-cf270b8a544a.jpg',
  },
  {
    id: 29,
    imageUrl: 'https://random.dog/f7a91550-018f-40df-9bd2-77e463a3917d.jpg',
  },
  {
    id: 30,
    imageUrl: 'https://random.dog/cdd5b5cc-f143-49ec-973f-3d401c6af23c.jpg',
  },
  {
    id: 31,
    imageUrl: 'https://random.dog/93ade0d8-ab54-4074-b7ba-89db44ec3b24.jpg',
  },
  {
    id: 32,
    imageUrl: 'https://random.dog/6c32c078-794b-4b95-bbc4-1f272c69486f.jpg',
  },
  {
    id: 33,
    imageUrl: 'https://random.dog/0fd781ff-ec46-4bdc-a4e8-24f18bf07def.jpg',
  },
  {
    id: 34,
    imageUrl: 'https://random.dog/561c2c37-9a6a-4efb-9ac1-b9ec62d1e106.jpg',
  },
  {
    id: 35,
    imageUrl: 'https://random.dog/41bf3852-811c-470f-8e3d-8b023e5e1c0b.jpg',
  },
  {
    id: 36,
    imageUrl: 'https://random.dog/1d9b4b44-e168-4b18-80d0-4919ad0e6fab.jpg',
  },
  {
    id: 37,
    imageUrl: 'https://random.dog/70201403-a7de-4524-879f-d42ae4f42e14.JPG',
  },
  {
    id: 38,
    imageUrl: 'https://random.dog/387c0fe7-b311-4a1b-b8c7-6667941e0e61.jpg',
  },
  {
    id: 39,
    imageUrl: 'https://random.dog/1a0535a6-ca89-4059-9b3a-04a554c0587b.jpg',
  },
  {
    id: 40,
    imageUrl: 'https://random.dog/07fcc953-0502-4cb6-bc0e-9b9d6904442d.jpg',
  },
  {
    id: 41,
    imageUrl: 'https://random.dog/bc1225bc-3d15-41c8-a0e3-36f47de9cc5f.jpg',
  },
  {
    id: 42,
    imageUrl: 'https://random.dog/16377a92-afe2-48ae-b540-37eec0ffec05.jpg',
  },
  {
    id: 43,
    imageUrl: 'https://random.dog/3a7ba33f-9818-4970-a713-db29fe43baa8.jpg',
  },
  {
    id: 44,
    imageUrl: 'https://random.dog/722a1a47-9a41-454f-bc65-8cb6c9613636.jpeg',
  },
  {
    id: 45,
    imageUrl: 'https://random.dog/42980553-2f4f-4829-86a0-1c9ba7b7300c.jpg',
  },
  {
    id: 46,
    imageUrl: 'https://random.dog/ea60dc85-258d-471b-8299-3fdb1c9e8319.jpg',
  },
  {
    id: 47,
    imageUrl: 'https://random.dog/382f720b-bfbe-4e3c-95d0-7aa2b00f4f4f.jpg',
  },
  {
    id: 48,
    imageUrl: 'https://random.dog/2a96966e-faea-4ab8-98a3-2914c28c44bb.jpg',
  },
  {
    id: 49,
    imageUrl: 'https://random.dog/00186969-c51d-462b-948b-30a7e1735908.jpg',
  },
  {
    id: 50,
    imageUrl: 'https://random.dog/63bb7640-708d-4bba-a6d5-af527b94254b.jpg',
  },
]

export default function handler(req, res) {
  const page = Number(req.query.page ?? 1)
  const limit = Number(req.query.limit ?? dogs.length)
  const items = dogs.slice(limit * (page - 1), limit * page)
  
  res.status(200).json(
    {
      items,
      meta: {
        page: Number(page),
        limit: Number(limit),
        total: dogs.length,
      }
    }
  )
}
